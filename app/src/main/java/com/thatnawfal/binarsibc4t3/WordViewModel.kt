package com.thatnawfal.binarsibc4t3

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class WordViewModel : ViewModel() {

    private val _aList : MutableLiveData<ArrayList<String>> = MutableLiveData()
    val aList: LiveData<ArrayList<String>> get() = _aList

    fun setWordList(aList: ArrayList<String>) {
        _aList.value = aList
    }
}