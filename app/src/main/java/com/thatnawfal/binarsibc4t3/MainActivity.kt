package com.thatnawfal.binarsibc4t3

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.thatnawfal.binarsibc4t3.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private val aWord = arrayListOf(
        Word('A', arrayListOf("Ayam", "Akar", "Abadi")),
        Word('B', arrayListOf("Bakar", "Buku", "Biawak")),
        Word('C', arrayListOf("Cukur", "Cicak", "Cantik", "Cukup", "Cadar", "Candu")),
        Word('D', arrayListOf("Dodol", "Dadu", "Dukun")),
        Word('E', arrayListOf("Ekor", "Empang", "Endok", "Ember")),
        Word('G', arrayListOf("Goreng", "Gagak", "Gulung", "Gajah")),
        Word('H', arrayListOf("Horden", "Hamba", "Harus")),
        Word('J', arrayListOf("Jera", "Jarum", "Jantung", "Jakun")),
        Word('L', arrayListOf("London", "Labil", "Loker", "Loreng")),
        Word('N', arrayListOf("Nakal", "Nirwana", "Nabi"))
    )

    private lateinit var binding: ActivityMainBinding
    private val viewModel: WordViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentCointainer, WordFragment())
            commit()
        }

        binding.button.setOnClickListener {
            binding.et.text?.let {
                wSetWordList(binding.et.text.single().uppercaseChar())
            }
        }
    }


    private fun wSetWordList(text: Char) {
        alphabetIsExcist(text)?.let { viewModel.setWordList(it) }
    }

    private fun alphabetIsExcist(alphabet: Char): ArrayList<String>? {
//        aWord.forEach { if (it.alphabets == alphabet) return it.listOfWord }
        var words: ArrayList<String>? = null
        for (i in aWord) {
            if (i.alphabets == alphabet) {
                words = i.listOfWord
                break
            }
        }
        return words
    }
}