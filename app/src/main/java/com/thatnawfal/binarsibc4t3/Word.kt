package com.thatnawfal.binarsibc4t3

data class Word(
    val alphabets: Char,
    val listOfWord: ArrayList<String>
)
